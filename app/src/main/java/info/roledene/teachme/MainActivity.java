package info.roledene.teachme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import info.roledene.teachme.model.Trending;
import info.roledene.teachme.pages.GigActivity;
import info.roledene.teachme.pages.MasterActivity;
import info.roledene.teachme.pages.MotionActivity;
import info.roledene.teachme.pages.ViewGigsActivity;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    TextView mTxtDisplay;
    Button clickBtn;
    String url = "https://api-v2launch.trakt.tv/movies/trending";
    RequestQueue queue;
    List<Trending> trendingList = new ArrayList<>();
    private String[] mPlanetTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private CharSequence mTitle;
    private CharSequence mDrawerTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        mTxtDisplay = (TextView) findViewById(R.id.helloTxt);
//        clickBtn = (Button) findViewById(R.id.clickBtn);
        queue = Volley.newRequestQueue(getApplicationContext());


        mPlanetTitles = new String[]{"item1", "item2", "item3"};
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        mDrawerList = (ListView) findViewById(R.id.left_drawer);
//
//        // Set the adapter for the list view
//        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
//                R.layout.fragment_navigation_drawer, mPlanetTitles));
//        // Set the list's click listener
//        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

//        clickBtn.setOnClickListener(this);


    }


    /**
     * Swaps fragments in the main content view
     */

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_home) {
            Intent intent = new Intent(MainActivity.this, MotionActivity.class);
            startActivity(intent);
            return true;
        }else if (id == R.id.action_my_question) {
            return true;
        } else if (id == R.id.action_ask_question) {
            Intent intent = new Intent(MainActivity.this, GigActivity.class);
            startActivity(intent);
            return true;
        }else if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_viewgigs) {
            Intent intent = new Intent(MainActivity.this, ViewGigsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case 1: {
                Intent intent = new Intent(MainActivity.this, MasterActivity.class);
                startActivity(intent);
                break;
            }

//            case R.id.clickButton2: {
//                // do something for button 2 click
//                break;
//            }

            //.... etc
        }
    }



}
