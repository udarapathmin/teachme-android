package info.roledene.teachme.pages;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import info.roledene.teachme.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecontFragment extends Fragment {


    public SecontFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_secont, container, false);
    }


}
