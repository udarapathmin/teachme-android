package info.roledene.teachme.pages;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import info.roledene.teachme.R;
import info.roledene.teachme.adapters.CustomListAdapter;
import info.roledene.teachme.api.VolleyCallback;
import info.roledene.teachme.model.Trending;
import info.roledene.teachme.network.RequestData;

public class TrendingActivity extends ActionBarActivity {

    String url = "https://api-v2launch.trakt.tv/movies/trending";
    RequestQueue queue;
    private List<Trending> tlist = new ArrayList<>();
    private ListView listView;
    private CustomListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending);
        queue = Volley.newRequestQueue(getApplicationContext());
        final Activity activity = this;
        RequestData requestData = new RequestData(this);

        requestData.getTrendingMoviesjsonRequest(new VolleyCallback() {
            @Override
            public void onSuccess(List<Trending> result) {
                ListAdapter listAdapter = new CustomListAdapter(activity, result);
//        ListAdapter listAdapter = new CustomAdapter(this,foods);
                ListView listView = (ListView) findViewById(R.id.trendingListView);
                listView.setAdapter(listAdapter);
            }


            @Override
            public void onError(String error) {
               Log.d("TAG","Error from callback : "+error);
            }
        });

    }


}
