package info.roledene.teachme.pages;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import info.roledene.teachme.R;
import info.roledene.teachme.adapters.GigAdapter;
import info.roledene.teachme.model.Gig;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewGagsFragment extends Fragment {

    ListView gigList;
    View view;

    public ViewGagsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_view_gags, container, false);


        Gig gig_data[] = {
                new Gig(1,2,"Looking for someone to teach me PHP","PHP, MySQL","Malabe,Sri Lanka","udara007","5 mins"),
                new Gig(2,1,"Java Guru Needed for 2 students","PHP, MySQL","Kandy,Sri Lanka","hasarel","10 mins"),
                new Gig(3,0,"Teach me office package","MSOffice, Desktop Publishing","Kandy,Sri Lanka","nerdStud","55 mins"),
                new Gig(4,1,"PHP Frameworks anyone?","CodeIgniter","Kandy,Sri Lanka","hackerx","1 hr"),
                new Gig(5,3,"SQL Tutor wanted!","XXX ZZZ ","Kandy,Sri Lanka","helloWorld1","2 hrs"),
                new Gig(6,4,"Hello World in 21 languages","YYYYYYYY","Kaduwela,Sri Lanka","SKRILLEX","5 hrs"),
                new Gig(7,2,"Kill mE","PHP, MySQL","Colombo 04,Sri Lanka","john_2","9 hrs"),
                new Gig(8,1,"LookingXXXXXXXXXXXXXX","XXXXXXXX","Galle, Sri Lanka","srii","10 hrs")
        };

        GigAdapter gigAdapter = new GigAdapter(getActivity().getApplicationContext(), R.layout.row_view_gig_single, gig_data);
//        Fragment fragment = new Fragment();
//        fragment = getsu
        gigList = (ListView) view.findViewById(R.id.gigList);
        gigList.setAdapter(gigAdapter);
        return view;
    }



/*
Gig gig_data[] = {
                new Gig(1,2,"Looking for someone to teach me PHP","PHP, MySQL","Malabe,Sri Lanka","udara007","5 mins"),
                new Gig(2,1,"Java Guru Needed for 2 students","PHP, MySQL","Kandy,Sri Lanka","hasarel","10 mins"),
                new Gig(3,0,"Teach me office package","MSOffice, Desktop Publishing","Kandy,Sri Lanka","nerdStud","55 mins"),
                new Gig(4,1,"PHP Frameworks anyone?","CodeIgniter","Kandy,Sri Lanka","hackerx","1 hr"),
                new Gig(5,3,"SQL Tutor wanted!","XXX ZZZ ","Kandy,Sri Lanka","helloWorld1","2 hrs"),
                new Gig(6,4,"Hello World in 21 languages","YYYYYYYY","Kaduwela,Sri Lanka","SKRILLEX","5 hrs"),
                new Gig(7,2,"Kill mE","PHP, MySQL","Colombo 04,Sri Lanka","john_2","9 hrs"),
                new Gig(8,1,"LookingXXXXXXXXXXXXXX","XXXXXXXX","Galle, Sri Lanka","srii","10 hrs")
        };

        GigAdapter gigAdapter = new GigAdapter(this.getActivity(),R.layout.row_view_gig_single, gig_data);

        gigList = (ListView)getView().findViewById(R.id.gigList);
        gigList.setAdapter(gigAdapter);
 */
}
