package info.roledene.teachme.pages;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import info.roledene.teachme.R;
import info.roledene.teachme.adapters.GigAdapter;
import info.roledene.teachme.model.Gig;

public class ViewGigsActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    ListView gigList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_gigs);

        Gig gig_data[] = {
            new Gig(1,2,"Looking for someone to teach me PHP","PHP, MySQL","Malabe,Sri Lanka","udara007","5 mins"),
            new Gig(2,1,"Java Guru Needed for 2 students","PHP, MySQL","Kandy,Sri Lanka","hasarel","10 mins"),
            new Gig(3,0,"Teach me office package","MSOffice, Desktop Publishing","Kandy,Sri Lanka","nerdStud","55 mins"),
            new Gig(4,1,"PHP Frameworks anyone?","CodeIgniter","Kandy,Sri Lanka","hackerx","1 hr"),
            new Gig(5,3,"SQL Tutor wanted!","XXX ZZZ ","Kandy,Sri Lanka","helloWorld1","2 hrs"),
            new Gig(6,4,"Hello World in 21 languages","YYYYYYYY","Kaduwela,Sri Lanka","SKRILLEX","5 hrs"),
            new Gig(7,2,"Kill mE","PHP, MySQL","Colombo 04,Sri Lanka","john_2","9 hrs"),
            new Gig(8,1,"LookingXXXXXXXXXXXXXX","XXXXXXXX","Galle, Sri Lanka","srii","10 hrs")
        };

        GigAdapter gigAdapter = new GigAdapter(this,R.layout.row_view_gig_single, gig_data);

        gigList = (ListView)findViewById(R.id.gigList);
        gigList.setAdapter(gigAdapter);
        gigList.setOnItemClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("TAG", "item clicked at " + position);
//        Intent intent = new Intent(this, GigActivity.class);
//        intent.putExtra("book", list.get(position).getId());
//        startActivityForResult(intent, 1);

    }
}
