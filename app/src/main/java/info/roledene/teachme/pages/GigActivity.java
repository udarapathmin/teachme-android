package info.roledene.teachme.pages;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import info.roledene.teachme.R;
import info.roledene.teachme.api.VolleyCallback;
import info.roledene.teachme.model.Trending;
import info.roledene.teachme.model.User;
import info.roledene.teachme.network.RequestData;

public class GigActivity extends ActionBarActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    TextView titleTxt;
    TextView detailsTxt;
    TextView catelogTxt;
    TextView tagTxt;
    ListView listView;
    Button postBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gig);
//        listView = (ListView) findViewById(R.id.gigList);

        titleTxt = (TextView) findViewById(R.id.titleTxt);
        detailsTxt = (TextView) findViewById(R.id.detailsTxt);
        catelogTxt = (TextView) findViewById(R.id.catalogTxt);
        tagTxt = (TextView) findViewById(R.id.tagTxt);
        postBtn = (Button) findViewById(R.id.postBtn);

        postBtn.setOnClickListener(this);
//        listView.setOnItemClickListener(this);

    }

//    OnItemClick

    @Override
    public void onClick(View v) {
//        Toast.makeText(this, "clicked", Toast.LENGTH_LONG);
        if (v == postBtn) {
            test();
            Log.d("TAG", "clicked");

        }
//        switch (v.getId()) {
//            case R.id.postBtn: {
////                test();
//                Toast.makeText(this, "postBtn", Toast.LENGTH_LONG);
//
//                break;
//            }
//            default:
//                Toast.makeText(this, "default", Toast.LENGTH_LONG);
//
//                break;
//        }
    }

    void test() {

//        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//
//        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
//                LOCATION_REFRESH_DISTANCE, mLocationListener);

        User user = new User();
        user.setUserId(1);
        user.setTitle(titleTxt.getText().toString());
        user.setDescription(detailsTxt.getText().toString());
        user.setCategory(catelogTxt.getText().toString());
        user.setLongitude((float) 34342.34);
        user.setLatitude((float) 334.34);
        user.setLocation("Matara");
        user.setTags(tagTxt.getText().toString());
        RequestData requestData = new RequestData(this);
        requestData.test(new VolleyCallback() {
            @Override
            public void onSuccess(List<Trending> result) {
                Log.d("TAG", "Successfully gig added");
            }

            @Override
            public void onError(String error) {
                Log.d("TAG", "error while adding the gig : " + error);
            }
        }, user);
        Toast.makeText(this, "botton clicked", Toast.LENGTH_LONG);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, GigActivity.class);
//        intent.putExtra("book", list.get(position).getId());
        startActivityForResult(intent, 1);
    }
}
