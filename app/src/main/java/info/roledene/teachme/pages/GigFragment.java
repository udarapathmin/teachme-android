package info.roledene.teachme.pages;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import info.roledene.teachme.R;
import info.roledene.teachme.api.VolleyCallback;
import info.roledene.teachme.model.Trending;
import info.roledene.teachme.model.User;
import info.roledene.teachme.network.RequestData;

/**
 * A simple {@link Fragment} subclass.
 */
public class GigFragment extends Fragment implements View.OnClickListener {

    View view;
    TextView titleTxt;
    Button postBtn;
    public GigFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gig, container, false);
        // Inflate the layout for this fragment
        titleTxt = (TextView) view.findViewById(R.id.titleTxt);
        postBtn = (Button) view.findViewById(R.id.postBtn);

//        postBtn.setOnClickListener(this);
        return inflater.inflate(R.layout.fragment_gig, container, false);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case  R.id.postBtn: {
                test();
                Toast.makeText(getActivity().getApplicationContext(), "postBtn", Toast.LENGTH_LONG);

                break;
            }
            default:
                Toast.makeText(getActivity().getApplicationContext(),"default",Toast.LENGTH_LONG);

                break;
    }

    }

    void test1(){

    }
    void test(){
        User user = new User();
        user.setUserId(1);
        user.setTitle(titleTxt.getText().toString());
        user.setDescription("test dscription");
        user.setCategory("test cat");
        user.setLongitude((float) 34342.34);
        user.setLatitude((float) 334.34);
        user.setLocation("Matara");
        user.setTags("java,php,ruby,c#");
        RequestData requestData = new RequestData(getActivity().getApplicationContext());
        requestData.test(new VolleyCallback() {
            @Override
            public void onSuccess(List<Trending> result) {
                Log.d("TAG","Successfully gig added");
            }

            @Override
            public void onError(String error) {
                Log.d("TAG","error while adding the gig : "+error);
            }
        }, user);
        Toast.makeText(getActivity().getApplicationContext(),"botton clicked",Toast.LENGTH_LONG);
    }
}
