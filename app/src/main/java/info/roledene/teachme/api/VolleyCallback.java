package info.roledene.teachme.api;

import java.util.List;

import info.roledene.teachme.model.Trending;

/**
 * Created by Roledene JKS on 10/7/2015.
 */
public interface VolleyCallback {
    void onSuccess(List<Trending> result);
    void onError(String error);
}
