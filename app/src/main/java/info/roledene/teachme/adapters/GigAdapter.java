package info.roledene.teachme.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import info.roledene.teachme.R;
import info.roledene.teachme.model.Gig;
import info.roledene.teachme.pages.GigViewActivity;


/**
 * Created by Lerasah on 11/5/2015.
 */
public class GigAdapter extends ArrayAdapter<Gig> implements View.OnClickListener {

    Context context;
    int layoutResourceId;
    Gig data[] = null;
    gigHolder holder = null;
    public GigAdapter(Context context, int layoutResourceId, Gig[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    private void vote(int oldI,int newI){

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;


        int votes=0;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new gigHolder();

            holder.txtTitle=(TextView)row.findViewById(R.id.txtTitle);
            holder.txtSkills=(TextView)row.findViewById(R.id.txtSkills);
            holder.textView=(TextView)row.findViewById(R.id.textView);
            holder.txtAgo=(TextView)row.findViewById(R.id.txtAgo);
            holder.txtLocation=(TextView)row.findViewById(R.id.txtLocation);
            holder.txtPoster=(TextView)row.findViewById(R.id.txtPoster);
            holder.btnDown=(Button)row.findViewById(R.id.btnDown);
            holder.btnView=(Button)row.findViewById(R.id.btnView);
            holder.ratingBar=(RatingBar)row.findViewById(R.id.ratingBar);
            holder.btnUp=(Button)row.findViewById(R.id.btnUp);

            row.setTag(holder);
        }
        else
        {
            holder = (gigHolder)row.getTag();
        }

        Gig gig = data[position];

        holder.txtTitle.setText(gig.getGigTitle());
        holder.txtSkills.setText(gig.getGigTags());
        votes = gig.getGigRating();
        holder.textView.setText(Integer.toString(votes));
        holder.txtLocation.setText((gig.getGigLocation()));
        holder.txtPoster.setText((gig.getGigPoster()));
        holder.txtAgo.setText(" (" + (gig.getGigTime()) + " ago)");

//        holder.btnDown.setOnClickListener(this);
        holder.btnDown.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view){
                Log.d("TAG", "down pressed");
                changeVote(-1);
            }
        });
        holder.btnUp.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View view){
                Log.d("TAG", "up pressed");
                changeVote(+1);
            }
        });
        holder.btnView.setOnClickListener(new TextView.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(getContext().getApplicationContext(),"Loading GIG",Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(context,GigViewActivity.class);
                myIntent.putExtra("gigID",holder.txtTitle.getText());
                context.startActivity(myIntent);
            }
        });
        holder.ratingBar.setStepSize(1);
        holder.ratingBar.setNumStars(5);
        holder.ratingBar.setMax(5);

        LayerDrawable stars = (LayerDrawable) holder.ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.rgb(255, 215, 0), PorterDuff.Mode.SRC_ATOP);
        //holder.ratingBar.setEnabled(false);
        holder.ratingBar.setClickable(false);
        holder.ratingBar.setRating(gig.getGigRating());

        holder.txtTitle.setOnClickListener(this);
        return row;
    }

//    @Override
//    public void onClick(View v) {
//            if(v == holder.btnUp ){
//                    test();
//            }
//    }

    void changeVote(int i) {
        holder.textView.setText(String.valueOf(Integer.parseInt(holder.textView.getText().toString()) + i));
        Log.d("TAG2", holder.textView.getText().toString());
        if (Integer.parseInt(holder.textView.getText().toString()) < 0) {
            //red
            holder.textView.setTextColor(Color.RED);
        } else {
            //default
            holder.textView.setTextColor(Color.BLACK);
        }
        if (i < 0) {
            holder.btnDown.setEnabled(false);
            holder.btnUp.setEnabled(true);
        } else {
            holder.btnUp.setEnabled(false);
            holder.btnDown.setEnabled(true);
        }
        this.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if (v == holder.txtTitle) {
//            Intent intent = new Intent(MainActivity.this, MotionActivity.class);
//            startActivity(intent);
        }
    }

    static class gigHolder
    {
        Button btnUp;
        Button btnDown;
        Button btnView;
        TextView txtTitle;
        TextView txtSkills;
        TextView txtLocation;
        TextView txtPoster;
        TextView textView;
        RatingBar ratingBar;
        TextView txtAgo;
    }
}
