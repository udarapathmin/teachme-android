package info.roledene.teachme.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import info.roledene.teachme.R;
import info.roledene.teachme.model.Trending;

/**
 * Created by Roledene JKS on 10/8/2015.
 */
public class CustomListAdapter extends BaseAdapter {
    private Activity activity;
    private List<Trending> trendingList;

    public CustomListAdapter(Activity activity,List<Trending> trendings){
        Log.d("TAGi","constructor called");
        this.activity = activity;
        this.trendingList = trendings;
    }

    @Override
    public int getCount() {
        Log.d("TAGi", "get count called");
        return trendingList.size();
    }

    @Override
    public Trending getItem(int position) {
        Log.d("TAGi","get item called");

        return trendingList.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.d("TAGi","get item id called");

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d("TAGi","get view called");

        LayoutInflater inflater = LayoutInflater.from(activity);
        View custView = inflater.inflate(R.layout.custom_row, parent, false);

//        String singleFoodItem = getItem(position);
        Trending t = trendingList.get(position);
        TextView textView = (TextView) custView.findViewById(R.id.text);
        ImageView imageView = (ImageView) custView.findViewById(R.id.image);

        textView.setText(t.getMovie().getTitle());
//        return super.getView(position, convertView, parent);
        return custView;
    }
}
