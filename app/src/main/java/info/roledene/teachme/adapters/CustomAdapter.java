package info.roledene.teachme.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import info.roledene.teachme.R;

/**
 * Created by Roledene JKS on 10/8/2015.
 */
public class CustomAdapter extends ArrayAdapter<String> {

   public CustomAdapter(Context context,String[] foods){
        super(context,R.layout.custom_row,foods);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View custView = inflater.inflate(R.layout.custom_row, parent, false);

        String singleFoodItem = getItem(position);
        TextView textView = (TextView) custView.findViewById(R.id.text);
        ImageView imageView = (ImageView) custView.findViewById(R.id.image);

        textView.setText(singleFoodItem);
//        return super.getView(position, convertView, parent);
        return custView;
    }
}
