package info.roledene.teachme.network;

/**
 * Created by Roledene JKS on 11/5/2015.
 */
public class Api {
    private static final String MOVIES_TRENDING = "https://api-v2launch.trakt.tv/movies/trending/";
    private static final String ADD_GIGS = "http://cf15.weclick.lk/index.php/api/Gig/addGig";

    public static String getAddGigs() {
        return ADD_GIGS;
    }

    public static String getTrendingShowsLink() {
        return MOVIES_TRENDING;
    }
}
