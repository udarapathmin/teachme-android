package info.roledene.teachme.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.roledene.teachme.api.VolleyCallback;
import info.roledene.teachme.model.RequestModel;
import info.roledene.teachme.model.Trending;
import info.roledene.teachme.model.User;

//import org.json.JSONArray;

//import org.json.JSONArray;

//import org.json.JSONArray;

//import org.json.JSONArray;

/**
 * <h1>RequestData</h1>
 * The request data class request the data from trakt.tv  api.
 * trakt.tv api give the data in json format via rest web service
 * <p>
 *
 * @author  Roledene JKS
 * @since   2015-11-04
 */
public class RequestData {
    private static final String TAG = "TAG";
    /**
     * RequestModel queue of the volley library
     */
    private static RequestQueue queue;
//    private static List<Trending> trendingList;

    public RequestData(Context applicationContext) {
        queue = Volley.newRequestQueue(applicationContext);

    }


    public void getTrendingMoviesjsonRequest(final VolleyCallback callback) {
        final String TAG = "TAG";
        String url = Api.getTrendingShowsLink();
        JsonArrayRequest jreq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                            Gson gson = new Gson();
                            Type trend = new TypeToken<Collection<Trending>>(){}.getType();
                            Collection<Trending> trendList = gson.fromJson(response.toString(), trend);
                            callback.onSuccess((List<Trending>) trendList);
                                for(Trending tt : trendList) {
                                    Log.d("TAGi", tt.getMovie().getTitle());
                                }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
                Log.d(TAG, "Error to string: " + error.toString());
                Log.d(TAG, "Error  stack trace: " + Arrays.toString(error.getStackTrace()));
                Log.d(TAG, "Error  status code: " + error.networkResponse.statusCode);
                Log.d(TAG, "Error  data: " + Arrays.toString(error.networkResponse.data));
                Log.d(TAG, "Error  cause: " + error.getCause());
//                pDialog.hide();
                Log.d(TAG, "Hide");
                callback.onError("Error getting trending movies");
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("trakt-api-key", "5713ccff0da9c0c502374f3def3ac7912df13f0d58a7ec91003eaff254d5ed84");
                headers.put("trakt-api-version", "2");
                return headers;
            }

        };
        queue.add(jreq);
    }

    public void addGig(final VolleyCallback callback, final User user) {
        String url = Api.getAddGigs();
        JsonArrayRequest jreq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

//                        Gson gson = new Gson();
//                        Type trend = new TypeToken<Collection<Trending>>(){}.getType();
//                        Collection<Trending> trendList = gson.fromJson(response.toString(), trend);

//                            Log.d(TAG, String.valueOf(i));
                        RequestModel request = new RequestModel();
                        try {
                            JSONObject jo = response.getJSONObject(0);
//                                Log.d(TAG,jo.toString());
                            request.setStatus(jo.getBoolean("Status"));
                            Log.d("TAG", String.valueOf(request.isStatus()));
//                                callback.onSuccess(request);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
                Log.d(TAG, "Error to string: " + error.toString());
                Log.d(TAG, "Error  stack trace: " + Arrays.toString(error.getStackTrace()));
                Log.d(TAG, "Error  status code: " + error.networkResponse.statusCode);
                Log.d(TAG, "Error  data: " + Arrays.toString(error.networkResponse.data));
                Log.d(TAG, "Error  cause: " + error.getCause());
                callback.onError("Error getting trending movies : ==> " + error.toString());
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("userID", String.valueOf(user.getUserId()));
                params.put("title", String.valueOf(user.getTitle()));
                params.put("description", String.valueOf(user.getDescription()));
                params.put("category", String.valueOf(user.getCategory()));
                params.put("longitude", String.valueOf(user.getLongitude()));
                params.put("latitude", String.valueOf(user.getLatitude()));
                params.put("location", String.valueOf(user.getLocation()));
                params.put("tags", String.valueOf(user.getTags()));
                return params;
            }
        };
        queue.add(jreq);
    }


    public void test(VolleyCallback callback, final User user) {
//        mPostCommentResponse.requestStarted();
//        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Api.getAddGigs(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                mPostCommentResponse.requestCompleted();
//                RequestModel request = new RequestModel();
//                try {
//                    JSONObject jo = response.getJSONObject(0);
////                                Log.d(TAG,jo.toString());
//                    request.setStatus(jo.getBoolean("Status"));
//                    Log.d("TAG",String.valueOf(request.isStatus()));
//                                callback.onSuccess(request);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("userID", String.valueOf(user.getUserId()));
                params.put("title", String.valueOf(user.getTitle()));
                params.put("description", String.valueOf(user.getDescription()));
                params.put("category", String.valueOf(user.getCategory()));
                params.put("longitude", String.valueOf(user.getLongitude()));
                params.put("latitude", String.valueOf(user.getLatitude()));
                params.put("location", String.valueOf(user.getLocation()));
                params.put("tags", String.valueOf(user.getTags()));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }
}


