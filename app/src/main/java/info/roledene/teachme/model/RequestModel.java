package info.roledene.teachme.model;

/**
 * Created by Roledene JKS on 11/5/2015.
 */
public class RequestModel {
    boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
