package info.roledene.teachme.model;

/**
 * Created by Lerasah on 11/5/2015.
 */
public class Gig {
    private int gigID;
    private int gigRating;
    private String gigTitle;
    private String gigDescription;
    private String gigCategory;
    private String gigLongitude;
    private String gigLatitude;
    private String gigLocation;
    private String gigTags;
    private String gigPoster;
    private String gigTime;

    public Gig(int gigID,int gigRating,String gigTitle,String gigTags,String gigDescription,String gigCategory,String gigLatitude,String gigLongitude,String gigLocation,String gigPoster){

    }

    public Gig(int GigID,int GigRating,String GigTitle,String GigTags,String GigLocation,String GigPoster,String GigTime){
        this.setGigID(GigID);
        this.setGigRating(GigRating);
        this.setGigTitle(GigTitle);
        this.setGigTags(GigTags);
        this.setGigLocation(GigLocation);
        this.setGigPoster(GigPoster);
        this.setGigTime(GigTime);
    }



    public int getGigID() {
        return gigID;
    }

    public void setGigID(int gigID) {
        this.gigID = gigID;
    }

    public String getGigTitle() {
        return gigTitle;
    }

    public void setGigTitle(String gigTitle) {
        this.gigTitle = gigTitle;
    }

    public String getGigDescription() {
        return gigDescription;
    }

    public void setGigDescription(String gigDescription) {
        this.gigDescription = gigDescription;
    }

    public String getGigCategory() {
        return gigCategory;
    }

    public void setGigCategory(String gigCategory) {
        this.gigCategory = gigCategory;
    }

    public String getGigLongitude() {
        return gigLongitude;
    }

    public void setGigLongitude(String gigLongitude) {
        this.gigLongitude = gigLongitude;
    }

    public String getGigLatitude() {
        return gigLatitude;
    }

    public void setGigLatitude(String gigLatitude) {
        this.gigLatitude = gigLatitude;
    }

    public String getGigLocation() {
        return gigLocation;
    }

    public void setGigLocation(String gigLocation) {
        this.gigLocation = gigLocation;
    }

    public String getGigTags() {
        return gigTags;
    }

    public void setGigTags(String gigTags) {
        this.gigTags = gigTags;
    }

    public int getGigRating() {
        return gigRating;
    }

    public void setGigRating(int gigRating) {
        this.gigRating = gigRating;
    }

    public String getGigPoster() {
        return gigPoster;
    }

    public void setGigPoster(String gigPoster) {
        this.gigPoster = gigPoster;
    }

    public String getGigTime() {
        return gigTime;
    }

    public void setGigTime(String gigTime) {
        this.gigTime = gigTime;
    }
}
