package info.roledene.teachme.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roledene JKS on 10/7/2015.
 */
public class Trending implements BaseModel {

    @SerializedName("watchers")
    private int watchers;

    @SerializedName("movie")
    private Movie movie;

    @SerializedName("ids")
    private Ids ids;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Ids getIds() {
        return ids;
    }

    public void setIds(Ids ids) {
        this.ids = ids;
    }

    public int getWatchers() {
        return watchers;
    }

    public void setWatchers(int watchers) {
        this.watchers = watchers;
    }

    public class Movie {

        @SerializedName("title")
        private String title;

        @SerializedName("year")
        private int year;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }
    }

    public class Ids {

        @SerializedName("trakt")
        private int trakt;

        @SerializedName("slug")
        private String slug;

        @SerializedName("imdb")
        private String imdb;

        @SerializedName("tmdb")
        private int tmdb;

        public int getTrakt() {
            return trakt;
        }

        public void setTrakt(int trakt) {
            this.trakt = trakt;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getImdb() {
            return imdb;
        }

        public void setImdb(String imdb) {
            this.imdb = imdb;
        }

        public int getTmdb() {
            return tmdb;
        }

        public void setTmdb(int tmdb) {
            this.tmdb = tmdb;
        }
    }

}
